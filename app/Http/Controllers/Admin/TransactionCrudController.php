<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TransactionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use Illuminate\Support\Facades\Auth;

/**
 * Class TransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Transaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/transaction');
        CRUD::setEntityNameStrings('transaction', 'transactions');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        CRUD::removeButtons(['create', 'update', 'delete']);

        CRUD::removeAllButtonsFromStack('line');

        CRUD::removeColumn('pos_id');
        
        CRUD::addColumn([
            'name' => 'created_at',
            'type' => 'datetime',
            'label' => 'Created'   
        ]);

        CRUD::addColumn([  
            'name'         => 'pos', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Pos', // Table column heading
         ])->makeFirstColumn();

         CRUD::setColumnDetails('nominal', [
            'name'  => 'nominal', // The db column name
            'label' => 'Nominal', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp. ',
            'thousands_sep' => ',',
         ]);

        CRUD::addClause('whereHas', 'pos', function($query){
            $query->where(['user_id' => Auth::guard('backpack')->user()->id]);
        });

        CRUD::enableDetailsRow();

        CRUD::addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'Filter Date'
          ],
          false,
          function ($value) {
            $dates = json_decode($value);
            $this->crud->addClause('where', 'created_at', '>=', $dates->from);
            $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
          });
    }

    public function showDetailsRow($id)
    {

        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;

        return view('crud::details_row', $this->data);
    }
}
