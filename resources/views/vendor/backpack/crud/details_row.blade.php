<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Product</th>
						<th scope="col">Qty</th>
						<th scope="col">Price</th>
					</tr>
				</thead>
				<tbody>
					@php $sum = 0; @endphp
					@foreach($entry->items as $key => $value)
					<tr>
						<td>{{ $key + 1 }}</td>
						<td>{{ $value->product->name }}</td>
						<td>{{ $value->quantity }}</td>
						<td>{{ 'Rp. '.number_format($value->product->price * $value->quantity) }}</td>
						@php $sum += $value->product->price * $value->quantity ; @endphp
					</tr>
					@endforeach
					<tr>
						<td colspan="3">Nominal</td>
						<td>{{ 'Rp. '.number_format($sum) }}</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
</div>
<div class="clearfix"></div>