<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function createTransaction(Request $request){
        try {
            $input = Validator::make($request->all(), [
                'nominal' => 'required|integer',
                'item' => 'required',
            ]);

            if ($input->fails()) {
                return $this->errorResponse($input->messages(), 400);
            }
            
            DB::beginTransaction();

            $transaction = new Transaction();
            $transaction->pos_id = $request->identity->id;
            $transaction->nominal = $request->nominal;
            $transaction->save();
            
            foreach ($request->item as $key => $item) {
               $trx_item = new TransactionItem();
               $trx_item->transaction_id = $transaction->id;
               $trx_item->product_id = $item['product_id'];
               $trx_item->quantity = $item['quantity'];
               $trx_item->save();
            }

            DB::commit();

            return $this->successResponse($transaction);

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e, 500);
        }
    }
}
