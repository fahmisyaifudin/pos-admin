<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pos;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Firebase\JWT\JWT;

class AuthController extends Controller
{
    protected function jwt(Pos $user) {
        $payload = [
            'iss' => "laravel-jwt", // Issuer of the token
            'sub' => $user->auth_key, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    public function login(Request $request){
        try {
            $input = Validator::make($request->all(), [
                'username' => 'required|string',
                'password' => 'required|string',
            ]);

            if ($input->fails()) {
                return $this->errorResponse($input->messages(), 400);
            }

            $pos = Pos::where('username', $request->username)->first();
            if (!$pos) {
                return $this->errorResponse('Username does not exist', 400);
            }

            if (Hash::check($request->password, $pos->password)) {
                return $this->successResponse(['user' => $pos, 'token' => $this->jwt($pos)]);  
            }
            
            return $this->errorResponse('Password or username wrong', 400);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }
}
