<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function successResponse($data = null)
    {
        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);
    }

    protected function errorResponse($message, $status)
    {
        return response()->json([
            'message' => $message
        ], $status);
    }
}
